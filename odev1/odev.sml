


fun number_in_month (date_list:(int*int*int) list,month:int)=
    if 
	null date_list
    then 
	0
    else
	let 
	    val x=
		if
		    #2 (hd date_list) = month 
		then
		    1
		else
		    0
	in
	    x+number_in_month(tl date_list,month)
	end

fun number_in_months (date_list:(int*int*int) list,month_list:int list)=
    if
	null month_list
    then 
	0
    else
	number_in_month (date_list,hd month_list)+number_in_months(date_list,tl month_list)


fun dates_in_month (date_list:(int*int*int) list,month:int)=
    if
	null date_list 
    then
	[]
    else
	let 
	    val result_of_rest=dates_in_month (tl date_list,month) 
	in
	    if
		#2 (hd date_list)= month 
	    then
		hd date_list::result_of_rest
	    else
		result_of_rest
		    
	end

fun dates_in_months (date_list:(int*int*int) list,month_list:int list)=
    if 
	null month_list 
    then 
	[] 
    else
	dates_in_month (date_list,hd month_list) @ dates_in_months(date_list,tl month_list)

fun get_nth (string_list:string list,n:int)=
    if 
	n=1 
    then
	hd string_list 
    else
	get_nth (tl string_list,n-1)

fun date_to_string (date:(int*int*int))=
    let
	val month_list=["January","February","March","April","May","June","July","August","September","Octoboer","November","December"]
    in
	get_nth(month_list,#2 date)
	^" "^
	Int.toString( #3 date)
	^", "^
	Int.toString( #1 date)
    end


fun number_before_reaching_sum (sum:int,number_list:int list)=
    if 
	sum<=hd number_list
    then
	0
    else
	1+number_before_reaching_sum(sum-hd number_list,tl number_list)

fun what_month (day_of_year:int)=
    let
	val int_list=[31,28,31,30,31,30,31,31,30,31,30,31]
    in 
	number_before_reaching_sum (day_of_year,int_list)
    end

fun month_range (day1:int,day2:int)=
    let
	val month_of_day1 = what_month day1
	val month_of_day2 = what_month day2
	fun count_from_to (from:int,to:int)=
	    if
		from > to
	    then 
		[]
	    else
		from::count_from_to(from+1,to)
    in
	count_from_to (month_of_day1,month_of_day2)
    end

fun oldest (list_of_dates:(int*int*int) list)=

    if 
	null list_of_dates
    then
	NONE
    else
	let
	    val max_of_rest=oldest( tl list_of_dates)
	in
	    if
		isSome max_of_rest andalso 
		is_older(hd list_of_dates,valOf max_of_rest)
	    then
		max_of_rest
	    else
		SOME (hd list_of_dates)
	end
